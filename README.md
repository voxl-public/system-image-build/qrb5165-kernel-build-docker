# qrb5165-kernel-build

QRB5165 Kernel Build Docker Image Project.

## Summary

This project is used to generate a docker image that can then be used as a build host.  This means you don't have to deal with setting up tools, which can be a great deal of pain!

It does mean you need to have a bare minimum knowledge of git, bash and Docker.

## Requirements

- Git
- Docker
- The more processors, the better!

## To Build Image

### Select Correct Versions

- From [VOXL2 Release Notes](https://docs.modalai.com/voxl2-voxl2-mini-system-image/#system-image-changelog) locate the version you want to build (e.g. SDK 1.3.3 uses System Image/Kernel Version v1.7.8)

### Sync Repo

```
git clone https://gitlab.com/voxl-public/system-image-build/qrb5165-kernel-build-docker.git
cd qrb5165-kernel-build-docker
```

### Update Tag

- Edit `MODAL_REPO_BRANCH` in `bin/qrb5165-common.sh`
- Using example above, set `MODAL_REPO_BRANCH=v1.7.8` to target the v1.7.8 system image/kernel for SDK 1.3.3 as described above, which will sync these:

```
https://gitlab.com/voxl-public/system-image-build/qrb5165-kernel/-/tags/v1.7.8
https://gitlab.com/voxl-public/system-image-build/meta-voxl2-bsp/-/tags/v1.7.8
https://gitlab.com/voxl-public/system-image-build/meta-voxl2/-/tags/v1.7.8
```

### Build Image

```
./docker-build-image.sh
```

## To Use

See [User Guide](https://docs.modalai.com/voxl2-kernel-build-guide).

## Quickstart

After building Docker container

```
./docker-run-image.sh

# now inside the container

./qrb5165-sync.sh
./qrb5165-patch.sh

# for VOXL2...
./qrb5165-build.sh -m M0054

# for VOXL2 Mini..
./qrb5165-build.sh -m M0104
```

### patches

These are generally patches used to fix the build, but some of these should likely be moved to something like [meta-voxl2](https://gitlab.com/voxl-public/system-image-build/meta-voxl2) or [meta-voxl2-bsp](https://gitlab.com/voxl-public/system-image-build/meta-voxl2-bsp).

### perf vs debug

The default build is "debug", which enables the serial debug console (info for [VOXL2](https://docs.modalai.com/voxl2-linux-user-guide/#serial-debug-console) and [VOXL2 Mini](https://docs.modalai.com/voxl2-mini-debug-console/)).

The debug build is good for troubleshooting, for example if the kernel hangs on a driver, you can see where it's hanging using the output of the serial console.

But, it makes boot up time MUCH slower (instead of the roughly 9 seconds for the "perf" kernel).

## Details

### docker-build-image.sh

Script that will generate a docker image, with name and version specified in `common`.

### docker-run-image.sh

Script that will start a container based on this docker image, mounting the current working directory into the workspace, giving you an interactive bash shell.

### common

A file called `common` lives in the root of the project:

```
# docker container meta
BUILD_OUTPUT=workspace
IMAGE_NAME=qrb5165-kernel-build
IMAGE_TAG=qrb5165-ubun1.0-14.1a
```

A few scripts source these values, so they're shared via sourcing this `common` file in the start of scripts.
#!/bin/bash

# Exit if any command has error
set -e

source qrb5165-common.sh

## -----------------------------------------------------------------------------------------------------------------------------
## Local Variables
## ----------------------------------------------------------------------------------------------------------------------------- 
PLATFORM=""
VARIANT=""
BUILD_MODE="perf"

## -----------------------------------------------------------------------------------------------------------------------------
## Help message print
## ----------------------------------------------------------------------------------------------------------------------------- 
_print_usage() {
	echo ""
	echo "Build QRB5165 kernel"
	echo "Usage ./qrb5165-build.sh --platform <PLATFORM> [--perf]"
	echo ""
	echo "Arguments"
	echo "	-h, --help      Show this help message and exit"
	echo ""
	echo "	-m, --machine   Select build machine (M0052 | M0054 | M0104)"
	echo "                  This argument is required"
	echo ""
	echo "	-v, --variant   Select build variant (var01 | var02)"
	echo "                  This argument is optional"
	echo ""
	echo "	-p, --perf      Enable perf build, by default debug kernel"
	echo "                  will build"
	echo ""
}


## -----------------------------------------------------------------------------------------------------------------------------
## Make sure all required areguments are set
## ----------------------------------------------------------------------------------------------------------------------------- 
_validate_args() {
	# Check for correct platform
	if [ "$PLATFORM" != "m0052" ] && [ "$PLATFORM" != "m0054" ] && [ "$PLATFORM" != "m0104" ]; then
		if [ "$PLATFORM" == "" ]; then
			echo "[ERROR] Selecting platform is required"
		else
			echo "[ERROR] Platform type is unsupported: $PLATFORM"
		fi
		
		_print_usage
		exit 1
	fi
}


## -----------------------------------------------------------------------------------------------------------------------------
## Print meta for simple user validation
## ----------------------------------------------------------------------------------------------------------------------------- 
_print_meta_info() {
	echo "[INFO] Starting Build"
	echo "[INFO]    Platform:   $PLATFORM"
	echo "[INFO]    Variant:    $VARIANT"
	echo "[INFO]    Build ID:   $BUILD_ID"
	echo "[INFO]    Build Type: $BUILD_MODE"

	sleep 2
}


## -----------------------------------------------------------------------------------------------------------------------------
## Parse arguments                            
## ----------------------------------------------------------------------------------------------------------------------------- 
_parse_opts() {
	while [[ $# -gt 0 ]]; do

		# convert argument to lower case for robustness
		arg=$(echo "$1" | tr '[:upper:]' '[:lower:]')

		# parse arguments
		case ${arg} in
			"h"|"-h"|"help"|"--help")
				_print_usage
				exit 0
				;;
			"m"|"-m"|"machine"|"--machine")
				PLATFORM=$(echo "$2" | tr '[:upper:]' '[:lower:]')
				shift
				;;
			"v"|"-v"|"variant"|"--variant")
				VARIANT=$(echo "$2" | tr '[:upper:]' '[:lower:]')
				shift
				;;
			"d"|"-d"|"debug"|"--debug")
				BUILD_MODE="debug"
				;;
			"p"|"-p"|"perf"|"--perf")
				BUILD_MODE="perf"
				;;
			*)
				echo "Unknown argument $1"
				_print_usage
				exit -1
				;;
		esac
		shift
	done

	# Make sure our args have been correctly populated, and print
	_validate_args
}

################################################################################
# Whitelists external variables for bitbake to use
################################################################################
add_external_env(){
	echo "[INFO] Updating environment variables to be used in the build"

	# export environmnet variables to be used in build
	export PLATFORM
	export VARIANT
	export PREBUILT_SRC_DIR="${APPS_ROOT}/prebuilt_HY11"

	# parsed by bitbake and included as external environment variables
	export BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE PLATFORM VARIANT"
}

## -----------------------------------------------------------------------------------------------------------------------------
## Main kernel build logic
## ----------------------------------------------------------------------------------------------------------------------------- 
_build_kernel() {
	# Declare paths
	WORKSPACE="$(pwd)"
	BUILD_MOUNT_POINT=${WORKSPACE}/build_mount
	APPS_ROOT=${BUILD_MOUNT_POINT}/${BUILD_ID}/apps_proc

	cd "${APPS_ROOT}" || exit 1

	dist=""
	if [ "$BUILD_MODE" == "perf" ]; then
		dist="qti-distro-ubuntu-fullstack-perf"
	else
		dist="qti-distro-ubuntu-fullstack-debug"
	fi

	# Begin build
	set +e # to allow rebuilds, unguard this call
	MACHINE=${PLATFORM} DISTRO=$dist source poky/qti-conf/set_bb_env.sh
	set -e # re-enable...

	# include external environment variables to be used in build
	add_external_env

	echo "[INFO] - building kernel"
	bitbake linux-msm -c cleanall
	bitbake linux-msm
	bitbake qti-ubuntu-robotics-image -fc do_make_bootimg
	echo "[INFO] - Done building kernel"
}

_parse_opts $@
_print_meta_info
_build_kernel

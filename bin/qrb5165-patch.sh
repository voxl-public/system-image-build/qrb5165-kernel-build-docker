#!/bin/bash

# Exit if any command has error
set -e

source qrb5165-common.sh

# Paths
WORKSPACE="$(pwd)"
PATCH_ROOT="${WORKSPACE}"/patches
APPS_ROOT="${WORKSPACE}"/build_mount/"${BUILD_ID}"/apps_proc


## -----------------------------------------------------------------------------------------------------------------------------
## Validate environment is setup correctly
## -----------------------------------------------------------------------------------------------------------------------------
_validate_setup() {
	if [ ! -d "${APPS_ROOT}" ]; then
	    echo "[ERROR] Missing apps_proc root"
		exit 1
	elif [ ! -d "${PATCH_ROOT}" ]; then
		echo "[ERROR] Missing patches root"
		exit 1
	fi

	echo "[INFO] All paths found"
}


## -----------------------------------------------------------------------------------------------------------------------------
## Apply patches to build
## -----------------------------------------------------------------------------------------------------------------------------
_apply_build_patches() {

	_validate_setup

	# Apply the changes
	echo "[INFO] - applying qrb5165-build-util patches to fix the poky build (generic)"
	
	# fix updates from CAF to CLO
	cd "${APPS_ROOT}"/poky/meta-qti-robotics
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-robotics_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-bsp
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-bsp_changes.patch
	cd "${APPS_ROOT}"/poky/meta-webkit
	patch -p1 < "${PATCH_ROOT}"/clo/meta-webkit_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-audio
	cp "${PATCH_ROOT}"/clo/0001-fix-mixer_get_array-failed-issue.patch "${APPS_ROOT}"/poky/meta-qti-audio/recipes/tinyalsa/files/0001-fix-mixer_get_array-failed-issue.patch
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-audio_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-ml
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-ml_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-bt
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-bt_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-gst
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-gst_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-data
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-data_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-adk
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-adk_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-security
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-security_changes.patch
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 < "${PATCH_ROOT}"/clo/meta-qti-ubuntu_changes.patch

	# Remove ROS 
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-REMOVE-ROS2.patch
	cd "${APPS_ROOT}"/poky/meta-qti-bsp
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-BSP-REMOVE-ROS2.patch
	cd "${APPS_ROOT}"/poky/meta-qti-robotics
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-ROBOTICS-REMOVE-ROS2.patch

	# Remove public key to allow for unsigned kernel and add custom kernel path
	cd "${APPS_ROOT}"/poky/meta-qti-bsp
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-BSP-REMOVE-PUBLIC-KEY.patch  
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-BSP-ADD-KERNEL-PATH.patch    

	# Update recipes to include newer version of packages
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-UPDATE-LIBEXPAT1.patch
	patch -p1 -uN <	"${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-UPDATE-LIBXML.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-UPDATE-GST-PLUGINS-GOOD.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-DEBDL-PREMIRROR.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-RM-SRC_URI-DLMGR.patch

	# Remove kernel config path so our machine can set it
	#cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	#patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-REMOVE-KERNEL-CONFIG-PATH.patch

	# Make kernel headers not include perf extension
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-REMOVE-PERF.patch

	cd "${APPS_ROOT}"/poky/meta-voxl2-bsp
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-VOXL2-BSP-ADD-QCLASSES.patch

	# Remove packages from core image install since we are only building kernel
	# and prop deps aren't available
	cd "${APPS_ROOT}"/poky/meta-qti-ubuntu
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-MODIFY-CORE-PKGS.patch
	patch -p1 -uN < "${PATCH_ROOT}"/14.1a-META-QTI-UBUNTU-RM-AUDIO-CORE-PKGS.patch
}

_apply_build_patches $@



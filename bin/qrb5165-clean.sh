#!/bin/bash

# Exit if any command has error
set -e

source qrb5165-common.sh

# Declare paths
WORKSPACE="$(pwd)"
BUILD_MOUNT_POINT=${WORKSPACE}/build_mount
APPS_ROOT=${BUILD_MOUNT_POINT}/${BUILD_ID}/apps_proc

cd "${APPS_ROOT}" || exit 1

rm -rf build-qti-distro-ubuntu-fullstack*
rm -rf sstate-cache
